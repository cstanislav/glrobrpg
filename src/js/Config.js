export class Config {
  static CIRCLE = Math.PI * 2;
  static SCREEN_FOV_VERT = 1.65; // Vertical (rad) inak 1.8
  static STRIP_WIDTH = 2;
  static CANVAS_UPSCALE = 2;
  static FLOOR_WIDTH = 1;
  static FLOOR_HEIGHT = 1;
  static Z_MIN = -2;
  static Z_MAX = 6;
  static TOTAL_UPSCALE = this.STRIP_WIDTH * this.CANVAS_UPSCALE;
}
