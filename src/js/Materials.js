import * as THREE from "three";
import { Material } from "./Material.js";

export class Materials {
  constructor() {
    this.loader = new THREE.TextureLoader();
    this.materials = [];
    this.loadMaterials();
  }

  loadTexture(id, options) {
    let texture = this.loader.load("assets/" + options["map"]);
    texture.magFilter = THREE.NearestFilter;
    texture.minFilter = THREE.NearestFilter;

    var material;
    if (!options["isSprite"]) {
      material = new THREE.MeshStandardMaterial({
        map: texture,
      });
    } else {
      material = new THREE.SpriteMaterial({
        map: texture,
      });
    }

    if (options["displacementBias"]) {
      material.displacementBias = options["displacementBias"];
    }

    if (options["displacementMap"]) {
      let dispMap = this.loader.load("assets/" + options["displacementMap"]);

      material.displacementMap = dispMap;
      material.displacementScale = options["displacementScale"];
      material.needsUpdate = true;
    }

    var segments;
    if (options["segments"]) {
      segments = options["segments"];
    } else {
      segments = 1;
    }

    this.materials[id] = new Material(material, segments);
  }

  getMaterial(id) {
    return this.materials[id];
  }

  loadMaterials() {
    this.loadTexture("BRICK", {
      map: "brick1.png",
      displacementMap: "brick1_disp.png",
      displacementScale: 0.02,
      displacementBias: 0,
      segments: 64,
    });
    this.loadTexture("CRATE", {
      map: "crwdl6.png",
      displacementMap: "crwdl6_disp.png",
      displacementScale: 0.02,
      displacementBias: -0.02,
      segments: 64,
    });
    this.loadTexture("DIRT", { map: "floor7_1.png" });
    this.loadTexture("GRASS", {
      map: "grass1.png",
      displacementMap: "grass1_disp.png",
      displacementScale: 0.1,
      displacementBias: 0,
      segments: 32,
    });
    this.loadTexture("COBBLE", { map: "mossrock.png" });
    this.loadTexture("PLAYER", {
      map: "playa1.png",
      isSprite: true,
    });
    this.loadTexture("GRAYGUY", {
      map: "cposa1.png",
      isSprite: true,
    });
  }
}
