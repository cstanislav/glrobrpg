export class Material {
  constructor(material, segments) {
    this.material = material;
    this.segments = segments;
  }

  getMaterial() {
    return this.material;
  }

  getSegments() {
    return this.segments;
  }
}
