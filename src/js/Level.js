import { Block } from "./Block.js";
import { Thing } from "./Thing.js";

export class Level {
  constructor(game) {
    this.game = game;
    this.playerX = 3;
    this.playerY = 3;
    this.playerZ = 1.65;
    this.playerRot = (Math.PI * 0) / 4;
    this.grid = [];
    this.things = [];
    this.camera = 0;
    this.sizeX = 32;
    this.sizeY = 24;
    this.sizeZ = 16;

    for (let x = 3; x < 32; x++) {
      let block = this.randomBlock();
      block.setNorthWall("BRICK");
      this.setBlock(x, 2, 2, block);
    }
    for (let x = 2; x < 10; x++) {
      this.setBlock(x, 2, 3, this.randomBlock());
    }

    var block = this.randomBlock();
    block.setTop("CRATE");
    block.setBottom("CRATE");
    this.setBlock(5, 5, 2, block);
    this.setBlock(1, 1, 2, block);
    this.setBlock(2, 2, 3, block);
    this.setBlock(3, 7, 3, block);

    for (let y = 2; y <= 5; y++) {
      block = this.randomBlock();
      block.setTop("CRATE");
      block.setBottom("BRICK");
      this.setBlock(5, y, 3, block);
    }

    for (let x = 0; x <= 35; x++) {
      for (let y = 0; y <= 20; y++) {
        block = this.getBlock(x, y, 1);
        if (block == undefined) {
          block = new Block();
          this.setBlock(x, y, 1, block);
        }
        block.setTop("GRASS");
      }
    }

    // for (var x = 0; x <= 1; x++) {
    // 	for (var y = 0; y <= 2; y++) {
    // 		var block = this.getBlock(x, y, 2);
    // 		if (block == undefined) {
    // 			block = new Block();
    // 			this.setBlock(x, y, 2, block);
    // 		}
    // 		block.setBottom("WOODWALL");
    // 	}
    // }

    // DARK
    // this.ceilColor = "#000000";
    // this.floorColor = "#000000";
    // this.floorColor2 = "#000000";
    // this.maxLight = 0.2;
    // this.lightRange = 2;
    // this.fogColor = "#000000";
    // this.fogStart = 1;
    // this.renderDistance = 4;

    // SANDSTORM
    // this.ceilColor = "#F2D45C";
    // this.floorColor = "#777777";
    // this.floorColor2 = "#F2D45C";
    // this.maxLight = .6;
    // this.lightRange = 6;
    // this.renderDistance = 40;
    // this.fogColor = "#F2D45C";
    // this.fogStart = 2;

    // FOGGY
    this.skybox = "SKYBOX";
    this.ceilColor = "#DEDEDE";
    this.floorColor = "#444444";
    this.floorColor2 = "#444444";
    this.maxLight = 0.4;
    this.lightRange = 6;
    this.fogColor = "#444444";
    this.fogStart = 4;
    this.renderDistance = 200;

    this.addThing(new Thing(4, 5, 2, "PLAYER"));
    this.addThing(new Thing(18, 16, 2, "PLAYER"));
    this.addThing(new Thing(18, 17, 2, "PLAYER"));
    this.addThing(new Thing(18, 18, 2, "PLAYER"));
    this.addThing(new Thing(18, 19, 2, "PLAYER"));
    this.addThing(new Thing(18, 20, 2, "PLAYER"));
    this.addThing(new Thing(18, 21, 2, "PLAYER"));
    this.addThing(new Thing(17, 16, 2, "PLAYER"));
    this.addThing(new Thing(16, 16, 2, "PLAYER"));
    this.addThing(new Thing(15, 16, 2, "PLAYER"));
    this.addThing(new Thing(17, 19, 2, "PLAYER"));
    this.addThing(new Thing(4, 3, 2, "GRAYGUY"));
    this.addThing(new Thing(4, 20, 3, "PLAYER"));
  }

  // Returns Block object if it exists, otherwise returns undefined
  getBlock(x, y, z) {
    if (this.grid[x] && this.grid[x][y] && this.grid[x][y][z]) {
      return this.grid[x][y][z];
    } else {
      return undefined;
    }
  }

  setBlock(x, y, z, block) {
    if (
      x >= 0 &&
      x < this.sizeX &&
      y >= 0 &&
      y < this.sizeY &&
      z >= 0 &&
      z < this.sizeZ
    ) {
      if (this.grid[x] == undefined) {
        this.grid[x] = [];
      }

      if (this.grid[x][y] == undefined) {
        this.grid[x][y] = [];
      }

      this.grid[x][y][z] = block;
    }
  }

  randomBlock() {
    return new Block(this.randomWall());
  }

  randomWall() {
    var textureIds = ["BRICK", "CRATE", "COBBLE"];

    var randIndex = Math.floor(Math.random() * textureIds.length);
    var textureId = textureIds[randIndex];
    return textureId;
  }

  update() {
    this.things.forEach(function (thing) {
      thing.update();
    });
  }

  addThing(thing) {
    this.things.push(thing);
  }
}
