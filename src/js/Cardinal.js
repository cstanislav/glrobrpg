export class Cardinal {
  directions = {
    east: 0,
    north: 1,
    west: 2,
    south: 3,
  };

  constructor(vert = 0, side = 0) {
    if (vert == 0 && side == 0) {
      this.direction = this.directions.south;
    } else if (vert == 0 && side == 1) {
      this.direction = this.directions.north;
    } else if (vert == 1 && side == 0) {
      this.direction = this.directions.west;
    } else {
      this.direction = this.directions.east;
    }
  }
}
