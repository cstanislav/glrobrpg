export class Wall {
  constructor(textureId, translucent) {
    this.textureId = textureId;
    this.translucent = translucent;
  }
}
