import { Level } from "./Level.js";
import { Debug } from "./Debug.js";
import { GUI } from "./GUI.js";
import { Player } from "./Player.js";
import { Camera } from "./Camera.js";
import { InputHandler } from "./InputHandler.js";

export class Game {
  constructor() {
    this.level = new Level(this);
    this.debug = new Debug("#debug", this);
    this.player = new Player(
      this.level.playerX,
      this.level.playerY,
      this.level.playerZ,
      this.level.playerRot
    );
    this.level.addThing(this.player);
    var camera = new Camera(
      100,
      2,
      0.01,
      this.level.renderDistance,
      this.player
    );
    this.level.camera = camera;
    this.level.addThing(camera);

    this.gui = new GUI(this, this.debug);

    this.input = new InputHandler(this);
  }

  update() {
    this.input.update();
    this.level.update();
    this.gui.update();
  }
}
