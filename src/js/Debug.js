export class Debug {
  constructor(object, game) {
    this.object = document.querySelector(object);
    this.game = game;
    this.lines = [];
  }

  push(line) {
    this.lines.push(line);
  }

  update() {
    this.object.innerHTML = "";
    this.render();
    this.lines = [];
  }

  render() {
    this.lines.forEach((line) => {
      this.object.innerHTML += line + "\n";
    });
  }
}
