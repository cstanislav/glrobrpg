import { Cardinal } from "./Cardinal.js";

export class Block {
  nWall = null;
  sWall = null;
  eWall = null;
  wWall = null;
  top = null;
  bottom = null;

  constructor(wall = undefined) {
    if (wall != undefined) {
      this.setNorthWall(wall);
      this.setSouthWall(wall);
      this.setEastWall(wall);
      this.setWestWall(wall);
    }

    this.cardinal = new Cardinal();
  }

  getWall(direction) {
    if (direction == this.cardinal.directions.north) {
      return this.getNorthWall();
    } else if (direction == this.cardinal.directions.south) {
      return this.getSouthWall();
    } else if (direction == this.cardinal.directions.east) {
      return this.getEastWall();
    } else {
      return this.getWestWall();
    }
  }

  getNorthWall() {
    return this.nWall;
  }
  getSouthWall() {
    return this.sWall;
  }
  getEastWall() {
    return this.eWall;
  }
  getWestWall() {
    return this.wWall;
  }
  getTop() {
    return this.top;
  }
  getBottom() {
    return this.bottom;
  }

  setNorthWall(wall) {
    this.nWall = wall;
  }
  setSouthWall(wall) {
    this.sWall = wall;
  }
  setEastWall(wall) {
    this.eWall = wall;
  }
  setWestWall(wall) {
    this.wWall = wall;
  }
  setTop(textureId) {
    this.top = textureId;
  }
  setBottom(textureId) {
    this.bottom = textureId;
  }
}
