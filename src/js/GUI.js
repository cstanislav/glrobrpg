import { Screen } from "./Screen.js";
import { Minimap } from "./Minimap.js";

export class GUI {
  constructor(game, debug) {
    this.game = game;
    this.screen = new Screen("#screen", game.level);
    this.minimap = new Minimap("#minimap", game.level);
    this.debug = debug;
  }

  update() {
    this.screen.update();
    this.minimap.update();
    this.debug.update();
  }
}
