import * as THREE from "three";
import { Thing } from "./Thing.js";
import { Config } from "./Config.js";

export class Camera extends Thing {
  constructor(fov, aspect, near, far, target = null) {
    super(0, 0);
    this.camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
    this.adjustFov(fov);
    this.target = target;
    this.angle = 0; // look up/down;
  }

  update() {
    if (this.target != null) {
      this.x = this.target.x;
      this.y = this.target.y;
      this.rot = this.target.rot;
      this.z = this.target.z + this.target.height;
      this.angle = this.target.angle;

      this.camera.position.x = this.x;
      this.camera.position.y = this.y;
      this.camera.position.z = this.z;
      this.camera.setRotationFromEuler(
        new THREE.Euler(
          Math.PI / 2 + this.angle,
          0,
          -Math.PI / 2 + this.rot,
          "ZXY"
        )
      );
    }
  }

  adjustFov(fov) {
    if (fov < Config.SCREEN_FOV_VERT) {
      // Portrait
      // rad -> deg conversion
      this.setHorizontalFov(Config.SCREEN_FOV_VERT * (180 / Math.PI));
    } else {
      // Landscape
      // rad -> deg conversion
      this.setVerticalFov(Config.SCREEN_FOV_VERT * (180 / Math.PI));
    }
  }

  setAspect(aspect) {
    this.camera.aspect = aspect;
    this.camera.updateProjectionMatrix();
  }

  setVerticalFov(fov) {
    this.camera.fov = fov;
    this.camera.updateProjectionMatrix();
  }

  setHorizontalFov(fov) {
    this.camera.fov =
      (Math.atan(Math.tan((fov * Math.PI) / 360) / this.camera.aspect) * 360) /
      Math.PI; // degrees
    this.camera.updateProjectionMatrix();
  }

  moveTo(x, y, rot) {
    this.x = x;
    this.y = y;
    this.rot = rot;
    this.target = null;
  }
}
