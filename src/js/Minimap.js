export class Minimap {
  constructor(object, level) {
    this.object = document.querySelector(object);
    this.ctx = this.object.getContext("2d");
    this.level = level;
    this.SCALE = 10;
    this.LINEWIDTH = 2;
  }

  adjustSize() {
    this.object.width = this.level.sizeX * this.SCALE;
    this.object.height = this.level.sizeY * this.SCALE;
  }

  clear() {
    this.ctx.clearRect(0, 0, this.object.width, this.object.height);
    this.ctx.fillStyle = "rgba(0, 0, 0, 0.5)";
    this.ctx.fillRect(0, 0, this.object.width, this.object.height);
  }

  xTransform(x) {
    return x;
  }

  yTransform(y) {
    return this.object.height - y;
  }

  drawWalls() {
    var lines = [];

    for (var x = 0; x < this.level.sizeX; x++) {
      for (var y = 0; y < this.level.sizeY; y++) {
        var block = this.level.getBlock(x, y, 2);

        if (block == undefined) {
          continue;
        }

        var wallWest = block.getWestWall();
        var wallEast = block.getEastWall();
        var wallNorth = block.getNorthWall();
        var wallSouth = block.getSouthWall();

        if (wallWest != undefined) {
          lines.push({
            x1: x * this.SCALE,
            y1: y * this.SCALE,
            x2: x * this.SCALE,
            y2: y * this.SCALE + this.SCALE - this.LINEWIDTH,
            color: "rgba(255, 255, 255, 0.5)",
          });
        }

        if (wallEast != undefined) {
          lines.push({
            x1: x * this.SCALE + this.SCALE - this.LINEWIDTH,
            y1: y * this.SCALE,
            x2: x * this.SCALE + this.SCALE - this.LINEWIDTH,
            y2: y * this.SCALE + this.SCALE - this.LINEWIDTH,
            color: "rgba(255, 255, 255, 0.5)",
          });
        }

        if (wallNorth != undefined) {
          lines.push({
            x1: x * this.SCALE,
            y1: y * this.SCALE + this.SCALE - this.LINEWIDTH,
            x2: x * this.SCALE + this.SCALE - this.LINEWIDTH,
            y2: y * this.SCALE + this.SCALE - this.LINEWIDTH,
            color: "rgba(255, 255, 255, 0.5)",
          });
        }

        if (wallSouth != undefined) {
          lines.push({
            x1: x * this.SCALE,
            y1: y * this.SCALE,
            x2: x * this.SCALE + this.SCALE - this.LINEWIDTH,
            y2: y * this.SCALE,
            color: "rgba(255, 255, 255, 0.5)",
          });
        }
      }
    }

    lines.forEach((line) => {
      this.ctx.strokeStyle = line.color ? line.color : "#FFFFFF";
      this.ctx.lineWidth = this.LINEWIDTH;

      this.ctx.beginPath();
      this.ctx.moveTo(this.xTransform(line.x1), this.yTransform(line.y1));
      this.ctx.lineTo(this.xTransform(line.x2), this.yTransform(line.y2));
      this.ctx.closePath();
      this.ctx.stroke();
    });
  }

  drawThings() {
    this.level.things.forEach((thing) => {
      this.ctx.fillStyle = "#00FF00";
      this.ctx.fillRect(
        this.xTransform(thing.x * this.SCALE),
        this.yTransform(thing.y * this.SCALE) - this.SCALE / 2,
        this.SCALE / 2,
        this.SCALE / 2
      );
    });
  }

  update() {
    this.adjustSize();
    this.clear();
    this.drawWalls();
    this.drawThings();
  }
}
