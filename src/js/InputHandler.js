export class InputHandler {
  constructor(game) {
    this.game = game;
    this.bindInputs(this);
  }

  bindInputs(input) {
    const container = document.querySelector("#container");
    const player = this.game.player;

    container.ontouchstart = function (event) {
      this.startX = event.touches[0].clientX;
      this.startY = event.touches[0].clientY;
      this.endX = false;
      this.endY = false;
    };

    container.ontouchmove = function (event) {
      this.endX = event.touches[0].clientX;
      this.endY = event.touches[0].clientY;
    };

    container.ontouchend = function () {
      if (!this.endX || !this.endY) {
        return;
      }

      let offsetX = this.endX - this.startX;
      let offsetY = this.endY - this.startY;

      if (Math.abs(offsetX) > Math.abs(offsetY)) {
        // Turn left/right
        if (offsetX > 0) {
          player.turnRight();
        } else {
          player.turnLeft();
        }
      } else {
        // Move forward/backward
        if (offsetY > 0) {
          player.moveBackward();
        } else {
          player.moveForward();
        }
      }
    };

    document.onkeydown = function (e) {
      // console.log(e.keyCode);
      e = e || window.event;
      // Which key was pressed?
      switch (e.keyCode) {
        // Up, move player forward
        case 38:
        case 87:
          input.keyUp = true;
          break;
        // Down, move player backward
        case 40:
        case 83:
          input.keyDown = true;
          break;
        // Left, rotate player left
        case 37:
        case 65:
          input.keyTurnLeft = true;
          break;
        // Right, rotate player right
        case 39:
        case 68:
          input.keyTurnRight = true;
          break;
        case 81:
          input.keyStrafeLeft = true;
          break;
        case 69:
          input.keyStrafeRight = true;
          break;
        case 82:
          input.keyLookUp = true;
          break;
        case 70:
          input.keyLookDown = true;
          break;
        case 84:
          input.keyFlyUp = true;
          break;
        case 71:
          input.keyFlyDown = true;
          break;
      }
    };
    // Stop the player movement/rotation
    // when the keys are released

    document.onkeyup = function (e) {
      e = e || window.event;
      switch (e.keyCode) {
        case 38:
        case 87:
          input.keyUp = false;
          break;
        // Down, move player backward
        case 40:
        case 83:
          input.keyDown = false;
          break;
        // Left, rotate player left
        case 37:
        case 65:
          input.keyTurnLeft = false;
          break;
        // Right, rotate player right
        case 39:
        case 68:
          input.keyTurnRight = false;
          break;
        case 81:
          input.keyStrafeLeft = false;
          break;
        case 69:
          input.keyStrafeRight = false;
          break;
        case 82:
          input.keyLookUp = false;
          break;
        case 70:
          input.keyLookDown = false;
          break;
        case 84:
          input.keyFlyUp = false;
          break;
        case 71:
          input.keyFlyDown = false;
          break;
      }
    };
  }

  update() {
    var player = this.game.player;
    if (!player.moving) {
      if (this.keyUp) {
        player.moveForward();
      }
      if (this.keyDown) {
        player.moveBackward();
      }
      if (this.keyTurnLeft) {
        player.turnLeft();
      }
      if (this.keyTurnRight) {
        player.turnRight();
      }
      if (this.keyStrafeLeft) {
        player.strafeLeft();
      }
      if (this.keyStrafeRight) {
        player.strafeRight();
      }

      if (this.keyLookUp) {
        player.lookUp();
      }
      if (this.keyLookDown) {
        player.lookDown();
      }
      if (this.keyFlyUp) {
        player.flyUp();
      }
      if (this.keyFlyDown) {
        player.flyDown();
      }
    }
  }
}
