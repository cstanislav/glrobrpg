import * as THREE from "three";
import { Materials } from "./Materials.js";
import { Config } from "./Config.js";

export class Screen {
  constructor(object, level) {
    this.canvas = document.querySelector(object);
    this.renderer = new THREE.WebGLRenderer({ canvas: this.canvas });
    this.ctx = this.canvas.getContext("2d");

    this.level = level;
    this.materials = new Materials();

    this.scene = new THREE.Scene();
    this.scene.background = new THREE.Color(0xb1b8ba);
    this.scene.fog = new THREE.Fog(this.scene.background, 0, 9);

    this.buildScene(this.scene, this.meshes);

    const light1 = new THREE.DirectionalLight(0xccaaaa, 1);
    light1.position.set(-1, 2, 4);
    this.scene.add(light1);

    const light2 = new THREE.AmbientLight(0x606060); // soft white light
    this.scene.add(light2);

    this.adjustSize(true);
  }

  buildScene(scene) {
    for (var x = 0; x <= this.level.sizeX; x++) {
      for (var y = 0; y <= this.level.sizeY; y++) {
        for (var z = 0; z <= this.level.sizeZ; z++) {
          const block = this.level.getBlock(x, y, z);

          if (block) {
            const top = block.getTop();
            const bottom = block.getBottom();
            const nWall = block.getNorthWall();
            const sWall = block.getSouthWall();
            const eWall = block.getEastWall();
            const wWall = block.getWestWall();

            if (top) {
              const material = this.materials.getMaterial(top);
              const geometry = new THREE.PlaneGeometry(
                1,
                1,
                material.getSegments(),
                material.getSegments()
              );

              const mesh = new THREE.Mesh(geometry, material.getMaterial());
              mesh.position.set(x, y, z + 0.5);

              scene.add(mesh);
            }

            if (bottom) {
              const material = this.materials.getMaterial(bottom);
              const geometry = new THREE.PlaneGeometry(
                1,
                1,
                material.getSegments(),
                material.getSegments()
              );
              geometry.rotateY(Math.PI);

              const mesh = new THREE.Mesh(geometry, material.getMaterial());
              mesh.position.set(x, y, z - 0.5);

              scene.add(mesh);
            }

            if (nWall) {
              const material = this.materials.getMaterial(nWall);
              const geometry = new THREE.PlaneGeometry(
                1,
                1,
                material.getSegments(),
                material.getSegments()
              );
              geometry.rotateX(-Math.PI / 2);

              const mesh = new THREE.Mesh(geometry, material.getMaterial());
              mesh.position.set(x, y + 0.5, z);

              scene.add(mesh);
            }

            if (sWall) {
              const material = this.materials.getMaterial(sWall);
              const geometry = new THREE.PlaneGeometry(
                1,
                1,
                material.getSegments(),
                material.getSegments()
              );
              geometry.rotateX(Math.PI / 2);

              const mesh = new THREE.Mesh(geometry, material.getMaterial());
              mesh.position.set(x, y - 0.5, z);

              scene.add(mesh);
            }

            if (eWall) {
              const material = this.materials.getMaterial(eWall);
              const geometry = new THREE.PlaneGeometry(
                1,
                1,
                material.getSegments(),
                material.getSegments()
              );
              geometry.rotateY(Math.PI / 2);
              geometry.rotateX(Math.PI / 2);

              const mesh = new THREE.Mesh(geometry, material.getMaterial());
              mesh.position.set(x + 0.5, y, z);

              scene.add(mesh);
            }

            if (wWall) {
              const material = this.materials.getMaterial(wWall);
              const geometry = new THREE.PlaneGeometry(
                1,
                1,
                material.getSegments(),
                material.getSegments()
              );
              geometry.rotateY(-Math.PI / 2);
              geometry.rotateX(Math.PI / 2);

              const mesh = new THREE.Mesh(geometry, material.getMaterial());
              mesh.position.set(x - 0.5, y, z);

              scene.add(mesh);
            }
          }
        }
      }
    }

    this.level.things.forEach((thing) => {
      if (!thing.textureId) {
        return;
      }
      const material = this.materials.getMaterial(thing.textureId);
      console.log(material);
      const sprite = new THREE.Sprite(material.getMaterial());
      sprite.position.set(thing.x, thing.y, thing.z);

      scene.add(sprite);
    });
  }

  render() {
    this.renderer.render(this.scene, this.level.camera.camera);
  }

  update() {
    this.adjustSize();
    this.render();
    this.level.game.debug.push(
      this.level.camera.x +
        " " +
        this.level.camera.y +
        " " +
        this.level.camera.z
    );
  }

  adjustSize(force = false) {
    if (
      !force &&
      this.windowWidth == window.innerWidth &&
      this.windowHeight == window.innerHeight &&
      this.windowHeight != undefined
    ) {
      return;
    }

    this.windowWidth = window.innerWidth;
    this.windowHeight = window.innerHeight;

    this.canvas.width = window.innerWidth / Config.CANVAS_UPSCALE;
    this.canvas.height = window.innerHeight / Config.CANVAS_UPSCALE;
    this.width = this.canvas.width;
    this.height = this.canvas.height;
    //this.ctx.imageSmoothingEnabled = true;

    let temp =
      (Math.tan(Config.SCREEN_FOV_VERT / 2) * this.width) / this.height;
    this.fov = 2 * Math.atan(temp);

    this.renderer.setSize(this.windowWidth, this.windowHeight, false);

    this.level.camera.setAspect(this.width / this.height);
    this.level.camera.adjustFov(this.fov);
  }
}
