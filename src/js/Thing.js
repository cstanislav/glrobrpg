export class Thing {
  constructor(x, y, z, textureId = null, radius = 0.1) {
    this.x = x;
    this.y = y;
    this.z = z;
    this.rot = 0;
    this.textureId = textureId;
    this.radius = radius;
  }

  update() {}
}
